﻿var acc = document.getElementsByClassName("accordion-button");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].onclick = function () {
        /* Toggle between adding and removing the "active" class,
        to highlight the button that controls the panel */
        this.classList.toggle("active");

        /* Toggle between hiding and showing the active panel */
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
            panel.style.display = "none";
            $('#accordion-btn').removeClass('open');
        } else {
            panel.style.display = "block";
            $('#accordion-btn').addClass('open');
        }
        return false;
    }
}

var accDialog = document.getElementsByClassName("accordion-button-dialog");
var i;

for (i = 0; i < accDialog.length; i++) {
    accDialog[i].onclick = function () {
        /* Toggle between adding and removing the "active" class,
        to highlight the button that controls the panel */
        this.classList.toggle("active");

        /* Toggle between hiding and showing the active panel */
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
            panel.style.display = "none";
            $('#accordion-btn-dialog').removeClass('open');
        } else {
            panel.style.display = "block";
            $('#accordion-btn-dialog').addClass('open');
        }
        return false;
    }
}
