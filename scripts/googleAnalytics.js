﻿// Google Analytics counter 

window.ga = window.ga || function () { (ga.q = ga.q || []).push(arguments) }; ga.l = +new Date;
ga('create', 'UA-84153533-1', 'auto');


function googleAnalyticsSend(id, carModelSysName, eventCategory, eventAction) {
    var eventLabel = '';

    if (carModelSysName != undefined && carModelSysName != '') {
        eventLabel += 'model/' + carModelSysName;

        if (id != undefined && id != '') {
            eventLabel += './' + id;
        }        
    }
    else {
        eventLabel += window.location.href;
    }
    ga('send', 'event', eventCategory, eventAction, eventLabel);
}