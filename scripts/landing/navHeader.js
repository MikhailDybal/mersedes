﻿
$(document).ready(function () {

 
    //tabs
    $('.tab-link').click(function () {
        var tabLink = $(this).parents('.tabs-block').find('.tab-link');
        var tabID = $(this).attr('id');
        var tabContent;
        tabLink.each(function () {
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
            }
        });
        $(this).addClass('active');
        $(this).parents('.tabs-block').find('.tab-content').each(function () {
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
            }
            if ($(this).data('tab') == tabID) {
                tabContent = $(this);
            }
        });
        tabContent.addClass('active');
    });

    // mobile menu
    var globalNav = $('.global-nav');

    $('#mobile-toggle').click(function () {
        $(this).toggleClass('active');
        $('.global-nav').toggleClass('active-menu');
        //$('html').toggleClass('no-slide');
    });

    $('.global-nav > li').hover(function () {
        $(this).siblings('li').find('> ul').fadeOut();
        $(this).find('> ul').fadeIn();
    }, function () {
        var ul = $(this);
        setTimeout(function () {
            ul.find('> ul').fadeOut(400);
        }, 400);
    });

    //var curHeight = window.innerHeight ? window.innerHeight : $(window).height();
    //curHeight = curHeight - $('.layout-header').height();
    //globalNav.css({
    //    'height': curHeight
    //});

    //gallery on class page - nav

    $('.gallery-nav-item').click(function () {
        var gallerySlide = $('.gallery-item');
        var navSlideM = $('.gallery-nav-item-m');
        var galleryID = $(this).data('id');
        var curSlide;
        var curSlideM;
        navSlideM.each(function () {
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
            }
            if ($(this).parents('.owl-item').hasClass('active')) {
                $(this).removeClass('active');
            }
            if ($(this).data('id') == galleryID) {
                curSlideM = $(this);
            }
        });
        gallerySlide.each(function () {
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
            }
            if ($(this).data('gallery-nav') == galleryID) {
                curSlide = $(this);
            }
        });
        //curSlide.addClass('active');
        //curSlideM.addClass('active');
        //curSlideM.parents('.owl-item').addClass('active');
    });

    //gallery on class page - nav - modal
    $('.gallery-nav-item-m').click(function () {
        var gallerySlide = $('.gallery-item');
        var navSlide = $('.gallery-nav-item-m');
        var galleryID = $(this).data('id');
        var curSlide;
        navSlide.each(function () {
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
            }
        });
        gallerySlide.each(function () {
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
            }
            if ($(this).data('gallery-nav') == galleryID) {
                curSlide = $(this);
            }
        });
        $(this).addClass('active');
        curSlide.addClass('active');
    });

    //gallery on class page - size modal
    $('.size-gallery-nav-item').click(function () {
        var gallerySlide = $('.size-gallery-item');
        var navSlide = $('.size-gallery-nav-item');
        var galleryID = $(this).attr('id');
        var curSlide;
        navSlide.each(function () {
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
            }
        });
        gallerySlide.each(function () {
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
            }
            if ($(this).data('gallery-size') == galleryID) {
                curSlide = $(this);
            }
        });
        $(this).addClass('active');
        curSlide.addClass('active');
    });

    function size_gallery() {
        $('.size-gallery-nav-item').click(function () {
            var gallerySlide = $('.size-gallery-item');
            var navSlide = $('.size-gallery-nav-item');
            var galleryID = $(this).attr('id');
            var curSlide;
            navSlide.each(function () {
                if ($(this).hasClass('active')) {
                    $(this).removeClass('active');
                }
            });
            gallerySlide.each(function () {
                if ($(this).hasClass('active')) {
                    $(this).removeClass('active');
                }
                if ($(this).data('gallery-size') == galleryID) {
                    curSlide = $(this);
                }
            });
            $(this).addClass('active');
            curSlide.addClass('active');
        });
    }
});