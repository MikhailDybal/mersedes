﻿    $('.footer-subscribe-email-field').keyup(function () {
        var input = document.querySelector('[class="footer-subscribe-button"]');
        if ($(this).val() != '') {
            var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
            if (pattern.test($(this).val())) {
                $(this).css({ 'border': '1px solid #00adef' });
                $(this).css({ 'color': '#00adef' });
                input.removeAttribute('disabled');
                
            } else {
                $(this).css({ 'border': '1px solid #ff0000' });
                $(this).css({ 'color': '#ff0000' });
                input.setAttribute('disabled', true);
            }
        } else {
            $(this).css({ 'border': '1px solid red' });
            $(this).css({ 'color': '#ff0000' });
            input.setAttribute('disabled', true);
        }
    });

function subscribeInNewTab(me) {
    var email = $('.footer-subscribe-email-field').val();
    if (email != '') {

        gtag('event', 'Отправка формы', {
            'event_category': 'Форма',
            'event_label': 'Будьте в курсе. Подписка на новостную рассылку'
        });

        var url = "../https@subscribe.mercedes-benz.ru/@xxx=" + email;
        var win = window.open(url, '_blank');
        win.focus();
    } else {
        $('.footer-subscribe-email-field').css({ 'border': '1px solid #ff0000' });
        $('.footer-subscribe-email-field').css({ 'color': '#ff0000' });
        $('.footer-subscribe-email-field').focus();
       // me.setAttribute('disabled', true);
    }
}