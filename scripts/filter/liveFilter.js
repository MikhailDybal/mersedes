﻿var sliderRange = $("#slider-range");
var form = $('#filterForm');

$(document).ready(function () {
    $('.selectpicker').selectpicker();
    //sliderRange.draggable();
    sliderRange.slider({
        range: true,
        min: parseInt(($("#slide-price-min").val())),
        max: parseInt($("#slide-price-max").val()) + 30000,
        values: [parseInt($("#stock-price-min").val()), parseInt($("#stock-price-max").val()) + 30000],
        step: 30000,
        slide: function (event, ui) {
            $("#stock-price-min").val(ui.values[0]);
            $("#stock-price-max").val(ui.values[1]);
        },
        stop: function (event, ui) {
            getCars();
        }
    });
    //$("#stock-price-min").val(sliderRange.slider("values", 0));
    //$("#stock-price-max").val(sliderRange.slider("values", 1));


    var timer;
    $(document).on('keyup', ".stock-price", function () {
        var $pricetextBox = $(this);
        var $body = $("body");
        clearTimeout(timer);
        timer = setTimeout(function () {
            var str = $pricetextBox.parents('form:first').serialize();
             $("body").addClass("loading");
            $.post(
            {
                url: $pricetextBox.data("url"),
                data:str
            }).done(function (data) {
                $('#carGrid').html(data);
                $("#searchPrice").val($pricetextBox.val()).focus();
            }).fail(function (jqXhr, textStatus) {
                console.log("Something went wrong: " + textStatus + ".");
            }).always(function () {
                 $body.removeClass("loading");
            });
        }, 700);
    });

    $('[name="ColorValueCbx"].carFilterColor').on('click', function () {
        
        var html = "<input name='ColorValueIds' value='" + $(this).val() + "' style='display:none'/>";

        if ($("input[name='ColorValueIds'][value='" + $(this).val() + "']").length == 0) {
            form.append(html);
            $(this).prop('checked', true);
        }
        else {
            $("input[name='ColorValueIds'][value='" + $(this).val() + "']").remove();
            $(this).prop('checked', false);
        }
        getCars();
    });

    $('[name="SalonFurnishCbx"].carFilterColor').on('click', function () {
        var html = "<input name='SalonFurnishIds' value='" + $(this).val() + "' style='display:none'/>";

       // alert($("input[name='SalonFurnishIds'][value='" + $(this).val() + "']").length);

        if ($("input[name='SalonFurnishIds'][value='" + $(this).val() + "']").length == 0) {
            form.append(html);
            $(this).prop('checked', true);
        }
        else {
            $("input[name='SalonFurnishIds'][value='" + $(this).val() + "']").remove();
            $(this).prop('checked', false);
        }
        getCars();
    });

    $('#classesList').change(function () {
        getCars();
    });
    $('#modelList').change(function () {
        getCars();
    });
    $('#bodyTypesList').change(function () {
        getCars();
    });
    $('#engineList').change(function () {
        getCars();
    });
    $('#salonTypes').change(function () {
        getCars();
    });
    $('#locations').change(function () {
        getCars();
    });
    $('#productionYears').change(function () {
        getCars();
    }); 
});

$("body").on('click', "#priceSort", function (event) {
    var priceDirection = $('#sortOptionSortDirection').val();
    priceDirection == "Ascending" ? priceDirection = "Descending" : priceDirection = "Ascending";
    $('#sortOptionSortDirection').val(priceDirection);
    $('#sortOptionColumnName').val("FullPrice");
    getCars();
});

$("body").on('click', "#specialPriceSort", function (event) {
    var priceDirection = $('#sortOptionSortDirection').val();
    priceDirection == "Ascending" ? priceDirection = "Descending" : priceDirection = "Ascending";
    $('#sortOptionSortDirection').val(priceDirection);
    $('#sortOptionColumnName').val("SpecialPrice");
    getCars();
});


function getCars() {
    
    form.submit();
    
};