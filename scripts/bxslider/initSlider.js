﻿$(document).ready(function () {

    $('.sliderOnMainAndSpecialPages').bxSlider({
        auto: ($('.sliderOnMainAndSpecialPages').children().length < 2) ? false : true,
        pager: false
    });

	if ($(window).width() <= '750'){
		var countImages = 1;
	}else{
		var countImages = $("#bxslider-carousel div").length;
	}

    var realThumbSlider = $("#bxslider-carousel").bxSlider({
        controls: true,
        minSlides: countImages,
        maxSlides: countImages,
        slideWidth: 156,
        slideMargin: 12,
        moveSlides: 1,
        pager: false,
        speed: 1000,
        infiniteLoop: false,
        hideControlOnEnd: true,
        onSlideBefore: function ($slideElement, oldIndex, newIndex) {
            /*$j("#sliderThumbReal ul .active").removeClass("active");
            $slideElement.addClass("active"); */

        }
    });

    var slider2 = $('.sliderOnCarMbClasssMbModelPages');
    if(slider2.find('> div').length > 1){
      var realSlider = slider2.bxSlider({
        auto: true,
        controls: false,
        pager: true,
        //onSlideBefore: function ($slideElement, oldIndex, newIndex) {
        //    changeRealThumb(realThumbSlider, newIndex);
        //}
      });
    }else{
      var realSlider = slider2.bxSlider({
        auto: false,
        controls: false,
        pager: true,
        onSlideBefore: function ($slideElement, oldIndex, newIndex) {
           changeRealThumb(realThumbSlider, newIndex);
        }
      });
    }

        linkRealSliders(realSlider, realThumbSlider);
    




    function linkRealSliders(bigS, thumbS) {
        $("#bxslider-carousel").on("click", "a", function (event) {
            bigS.stopAuto();
            event.preventDefault();
            var newIndex = $(this).parent().attr("data-slideIndex");
            bigS.goToSlide(newIndex);
        });
    }

    function changeRealThumb(slider, newIndex) {
        var $thumbS = $("#bxslider-carousel");
        $thumbS.find('.active').removeClass("active");
        $thumbS.find('div[data-slideIndex="' + newIndex + '"]').addClass("active");
        if (slider.getSlideCount() - newIndex >= countImages) slider.goToSlide(newIndex);
        else slider.goToSlide(slider.getSlideCount() - countImages);
    }
});