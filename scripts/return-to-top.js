﻿$(function () {

    $(document).on('scroll', function () {

        if ($(window).scrollTop() > 100) {
            $('.scroll-top-wrapper').addClass('show');
        } else {
            $('.scroll-top-wrapper').removeClass('show');
        }
    });

    $('.scroll-top-wrapper').on('click', scrollToTop);
});

function scrollToTop() {
    verticalOffset = typeof (verticalOffset) != 'undefined' ? verticalOffset : 0;
    element = $('body');
    offset = element.offset();
    offsetTop = offset.top;
    $('html, body').animate({ scrollTop: offsetTop }, 500, 'linear');
}

$('.btn.btn-link.spoiler.mobile-spoiler').on('click', function() {
  if ($(this).find('.show-on-open').is(':visible'))
  {
     $('.show-on-open').hide();
     $('.show-on-close').show();
     $('.spoiler-content').removeClass('mobile-spoiler-content');
  }else{
     $('.show-on-open').show();
     $('.show-on-close').hide();
     $('.spoiler-content').addClass('mobile-spoiler-content');
  }
});