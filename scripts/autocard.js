$(document).ready(function () {
    var $body = $('body');
    
    $body.delegate('a.dialog', 'click', function () {
        var id = $(this).attr('data-id');
        var dialogPanel = $("#" + id);
        var dialog = dialogPanel.dialog({
            title: $(this).attr('data-dialogtitle'),
            autoOpen: true,
            resizable: false,
            minWidth: 600,
            minHeight: 230,
            modal: true
        });

        dialog.bind('clickoutside', function () {
            dialog.dialog('close');
        });
        return false;
    });

});